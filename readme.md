# Guess a number
This is a cross-platform (Android and iOS) application written in JavaScript utilising React and React Native.
It is based on content from the Udemy course found at: https://www.udemy.com/course/react-native-the-practical-guide/ \

## App description
The application is a small game with the simple objective of choosing a number between 1 and 99, and then having the computer guess the number.
For every guess, the user has to inform the computer whether the true number is greater or lower than what was guessed.

The game in itself is not meant to be entertaining, but simply to be a demonstration of React and React Native core features. 


## Prerequisites 
- Installed Expo
- Installed JS
- Installed React Native
- Installed React
- Installed either Android Studio or x-code for simulation, or have a phone with the Expo app installed


## Set-up
With all prerequisites ready, the app is simply run by navigating to the local repository and running `expo start`.
Afterwards the application is accessible in either Android or iOS simulators (Check Expo docs for more info) or through scanning the QR code with personal phone. 

## Photos
![alt text](./images/screen1.PNG)<div style="page-break-after: always;"></div>
![alt text](./images/screen2.PNG)<div style="page-break-after: always;"></div>
![alt text](./images/screen3.PNG)<div style="page-break-after: always;"></div>
![alt text](./images/screen4.PNG)<div style="page-break-after: always;"></div>
![alt text](./images/screen5.PNG)<div style="page-break-after: always;"></div>