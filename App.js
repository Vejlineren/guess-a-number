import React, { useState } from "react";
import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";

import Header from "./components/Header";
import StartGameScreen from "./screens/StartGameScreen";
import GameScreen from "./screens/GameScreen";
import GameOverScreen from "./screens/GameOverScreen";

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf")
  });
};

export default function App() {
  const [userNumber, setUserNumber] = useState(false);
  const [userReqReset, setUserReqReset] = useState(false);
  const [guessRounds, setGuessRounds] = useState(0);
  const [dataLoaded, setDataLoaded] = useState(false);

  // Initialise loading screen and load prerequisites (fonts)
  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setDataLoaded(true);
        }}
        onError={err => {
          console.log(err);
        }}
      ></AppLoading>
    );
  }

  const startGameHandler = selectedNumber => {
    setUserNumber(selectedNumber);
    setUserReqReset(false);
    setGuessRounds(0);
  };

  const configNewGameHandler = () => {
    setGuessRounds(0);
    setUserNumber(null);
  };

  const resetHandler = resetState => {
    setUserReqReset(resetState);
  };

  const gameOverHandler = numOfRounds => {
    setGuessRounds(numOfRounds);
  };
  let currentScreen = (
    <StartGameScreen onStartGame={startGameHandler}></StartGameScreen>
  );

  if (userNumber && guessRounds === 0) {
    currentScreen = (
      <GameScreen
        userChoice={userNumber}
        onReset={resetHandler}
        onGameOver={gameOverHandler}
      ></GameScreen>
    );
  } else if (guessRounds > 0) {
    currentScreen = (
      <GameOverScreen
        roundsNumber={guessRounds}
        userNumber={userNumber}
        onRestart={configNewGameHandler}
      ></GameOverScreen>
    );
  }

  if (userReqReset) {
    currentScreen = (
      <StartGameScreen onStartGame={startGameHandler}></StartGameScreen>
    );
  }

  return (
    <SafeAreaView style={styles.screen}>
      <Header title="Guess a Number" />
      {currentScreen}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1
  }
});
