// Basic layout for new component
import React, { useState } from "react";

import { StyleSheet } from "react-native";

const ClassName = props => {};

const styles = StyleSheet.create({});

export default ClassName;

// Custom Component Imports
import Card from "../components/Card";
import Colors from "../constants/colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import TitleText from "../components/TitleText";
import BodyText from "../components/BodyText";
import MainButton from "../components/MainButton";

// Dynamic screen size calculations
// var screenWidthConst = Dimensions.get("window").width;
// var screenHeightConst = Dimensions.get("window").height;

// // Manage screen orientation for dynamic size calculation
// const [screenWidth, setScreenWidth] = useState();
// const [screenHeight, setScreenHeight] = useState();

// useEffect(() => {
//   // // First, create a function that updates screenwidth and screenheight
//   const updateLayout = () => {
//     setScreenWidth(Dimensions.get("window").width);
//     setScreenHeight(Dimensions.get("window").height);

//     screenWidthConst = screenWidth;
//     screenHeightConst = screenHeight;
//   };

//   console.log(screenWidthConst);
//   console.log(screenHeightConst);

//   // Call update function when orientation changes
//   Dimensions.addEventListener("change", updateLayout);

//   // Return a "cleanup" function that clears the event handler
//   return () => {
//     Dimensions.removeEventListener("change", updateLayout);
//   };
// });
