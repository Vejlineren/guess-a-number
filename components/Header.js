import React from "react";

import { StyleSheet, View, Text, Platform } from "react-native";
import Colors from "../constants/colors";

const Header = props => {
  return (
    <View style={styles.header}>
      <Text
        style={{
          ...styles.headerTitleBase,
          ...Platform.select({
            ios: styles.headerTitleIOS,
            android: styles.headerTitleAndroid
          })
        }}
      >
        {props.title}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 90,
    padding: 36,
    backgroundColor: Colors.secondary,
    alignItems: "center",
    justifyContent: "center"
  },
  // Platform specific styling
  headerTitleBase: {
    fontSize: 18,
    fontFamily: "open-sans-bold"
  },
  headerTitleAndroid: {
    color: "black"
  },
  headerTitleIOS: {
    color: "white"
  }
});

export default Header;
