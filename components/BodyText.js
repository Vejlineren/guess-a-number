import React, { useState } from "react";

import { StyleSheet, Text } from "react-native";

const BodyText = props => {
  return (
    <Text style={{ ...styles.main, ...props.style }}>{props.children}</Text>
  );
};

const styles = StyleSheet.create({
  main: {
    fontFamily: "open-sans"
  }
});

export default BodyText;
