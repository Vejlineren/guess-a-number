import React, { useState } from "react";

import { StyleSheet, Text } from "react-native";

const TitleText = props => {
  return (
    <Text style={{ ...styles.main, ...props.style }}>{props.children}</Text>
  );
};

const styles = StyleSheet.create({
  main: {
    fontFamily: "open-sans-bold",
    fontSize: 18
  }
});

export default TitleText;
