import React from "react";

import { StyleSheet, View } from "react-native";

const Card = props => {
  return (
    <View style={{ ...styles.card, ...props.style }}>{props.children}</View>
  );
  // This View, uses the spread operator "..." to take the styling options defined in the styles.card style and combine them with the styles defined in props.style. As the props.style is given as the second argument, these styles will overwrite potential styles given in the styles.card.
};

const styles = StyleSheet.create({
  card: {
    // General
    backgroundColor: "white",
    padding: 10,
    borderRadius: 10, // Make rounded corners for the box

    // Shadow properties for iOS
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,

    // Shadow properties for Android
    elevation: 5
  }
});

export default Card;
