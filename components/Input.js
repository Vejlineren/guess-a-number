import React from "react";

import { StyleSheet, TextInput } from "react-native";

const Input = props => {
  return (
    <TextInput
      {...props}
      style={{ ...styles.input, ...props.style }}
    ></TextInput>
  );
  // The {...props} thing ensures that the custom component implementing this one, can use all the other properties that are not specifically defined in this class
};

const styles = StyleSheet.create({
  input: {
    height: 30,
    borderBottomColor: "grey",
    borderBottomWidth: 1,
    marginVertical: 10
  }
});

export default Input;
