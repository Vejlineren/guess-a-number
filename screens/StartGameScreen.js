import React, { useState } from "react";

import {
  StyleSheet,
  View,
  Text,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert
} from "react-native";

// Custom component imports
import Card from "../components/Card";
import Colors from "../constants/colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import TitleText from "../components/TitleText";
import BodyText from "../components/BodyText";
import MainButton from "../components/MainButton";

const StartGameScreen = props => {
  const [enteredValue, setEnteredValue] = useState(""); // Define state for entered value
  const [confirmed, setConfirmed] = useState(false); // Define state for confirmed button
  const [chosenNumber, setChosenNumber] = useState(); // Define state for the chosen number

  const numberInputHandler = inputText => {
    setEnteredValue(inputText.replace(/[^0-9]/g, "")); // Replace all inputs thar are not a number betweeen 0 and 9 with an empty string
  };

  const resetInputHandler = () => {
    setEnteredValue("");
    setConfirmed(false);
  };

  const confirmInputHandler = () => {
    const number = parseInt(enteredValue);
    if (isNaN(number) || number <= 0 || number > 99) {
      // If number is still not a number or = 0, then give error
      Alert.alert(
        "Invalid number",
        "Number has to be a number between 1 and 99",
        [{ text: "Okay", style: "destructive", onPress: resetInputHandler }]
      );
    }
    setConfirmed(true);
    setChosenNumber(number);
    setEnteredValue("");
    Keyboard.dismiss();
  };

  // Logic for visual feedback to the confirm button
  let confirmedOutput;

  if (confirmed) {
    confirmedOutput = (
      <Card style={styles.summaryContainer}>
        <Text>You selected</Text>
        <NumberContainer>{chosenNumber}</NumberContainer>
        <MainButton onPress={() => props.onStartGame(chosenNumber)}>
          START GAME
        </MainButton>
      </Card>
    );
  }

  return (
    <TouchableWithoutFeedback // Wrapping the entire view inside this special component, allows us to ensure that the keyboard is dismissed if the user clicks somewhere black on the page
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View style={styles.screen}>
        <TitleText style={styles.title}>The Game screen</TitleText>
        <Card style={styles.inputContainer}>
          <BodyText>Select a number</BodyText>
          <Input
            style={styles.input}
            blurOnSubmit
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="number-pad"
            maxLength={2}
            //Logic for handling user input
            onChangeText={numberInputHandler}
            value={enteredValue}
          ></Input>
          <View style={styles.buttonContainer}>
            <View style={styles.button}>
              <Button
                title="Reset"
                onPress={resetInputHandler}
                color={Colors.primary}
              ></Button>
            </View>
            <View style={styles.button}>
              <Button
                title="Confirm"
                onPress={confirmInputHandler}
                color={Colors.secondary}
              ></Button>
            </View>
          </View>
        </Card>
        {confirmedOutput}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center"
  },

  title: {
    marginVertical: 10
  },

  buttonContainer: {
    flexDirection: "row", // Ensure that the buttons inside the container are positioned next to eachother
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15
  },

  inputContainer: {
    width: "80%",
    maxWidth: "95%",
    minWidth: 300,
    alignItems: "center"
  },

  input: {
    width: 25,
    textAlign: "center"
  },

  summaryContainer: {
    margin: 10,
    alignItems: "center"
  }
});

export default StartGameScreen;
