import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  Button,
  Alert,
  ScrollView,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

// Custom Component Imports
import Card from "../components/Card";
import Colors from "../constants/colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import TitleText from "../components/TitleText";
import BodyText from "../components/BodyText";
import MainButton from "../components/MainButton";

const renderListItem = (value, numOfRounds) => {
  return (
    <View key={value} style={styles.listItem}>
      <BodyText>
        Round: <Text style={styles.highlight}>#{numOfRounds}</Text>
      </BodyText>
      <BodyText>
        Guess: <Text style={styles.highlight}>{value}</Text>
      </BodyText>
    </View>
  );
};

const generateRandomBetween = (min, max, exclude) => {
  min = Math.ceil(min);
  max + Math.floor(max);
  const randomNum = Math.floor(Math.random() * (max - min)) + min;
  if (randomNum === exclude) {
    return generateRandomBetween(min, max, exclude);
  } else {
    return randomNum;
  }
};

var screenWidthConst = Dimensions.get("window").width;
var screenHeightConst = Dimensions.get("window").height;

const GameScreen = props => {
  const initialGuess = generateRandomBetween(1, 100, props.userChoice);
  const [currentGuess, setCurrentGuess] = useState(initialGuess);
  const [pastRounds, setPastRounds] = useState([initialGuess]);
  const currentLow = useRef(1);
  const currentHigh = useRef(100);

  // Manage screen orientation for dynamic size calculation
  const [screenWidth, setScreenWidth] = useState();
  const [screenHeight, setScreenHeight] = useState();

  useEffect(() => {
    // // First, create a function that updates screenwidth and screenheight
    const updateLayout = () => {
      setScreenWidth(Dimensions.get("window").width);
      setScreenHeight(Dimensions.get("window").height);

      screenWidthConst = screenWidth;
      screenHeightConst = screenHeight;
    };

    console.log(screenWidthConst);
    console.log(screenHeightConst);

    // Call update function when orientation changes
    Dimensions.addEventListener("change", updateLayout);

    // Return a "cleanup" function that clears the event handler
    return () => {
      Dimensions.removeEventListener("change", updateLayout);
    };
  });

  const { userChoice, onGameOver } = props; // Props destructuring

  useEffect(() => {
    if (currentGuess === userChoice) {
      onGameOver(pastRounds.length);
    }
  }, [currentGuess, userChoice, onGameOver]);

  const nextGuessHandler = direction => {
    if (
      (direction === "lower" && currentGuess < props.userChoice) ||
      (direction === "greater" && currentGuess > props.userChoice)
    ) {
      Alert.alert("Don't lie!", "You have to be honest in this game...", [
        { text: "Alright!", style: "cancel" }
      ]);
      return;
    }

    if (direction === "lower") {
      currentHigh.current = currentGuess; // If you press the lower button the current guess  must be the highest possible number
    } else if (direction === "greater") {
      currentLow.current = currentGuess + 1; // If you press the greater button, the current guess must be the lowest possible number
    }

    const nextNumber = generateRandomBetween(
      currentLow.current,
      currentHigh.current,
      currentGuess
    );
    setCurrentGuess(nextNumber);
    //setCurRounds(curRounds => curRounds + 1);
    setPastRounds([nextNumber, ...pastRounds]);
  };
  // <Ionicons name="md-remove" color="white" size={24} />
  return (
    <View style={styles.screen}>
      <Text>Computer's guess</Text>
      <NumberContainer>{currentGuess}</NumberContainer>
      <Card style={styles.buttonContainer}>
        <MainButton
          onPress={nextGuessHandler.bind(this, "lower")}
          viewStyle={styles.removeButton}
        >
          <Ionicons name="md-remove" color="white" size={24} />
        </MainButton>
        <MainButton onPress={nextGuessHandler.bind(this, "greater")}>
          <Ionicons name="md-add" color="white" size={24} />
        </MainButton>
      </Card>
      <Button
        title="Reset"
        color="red"
        onPress={() => props.onReset(true)}
      ></Button>
      <View style={styles.box}>
        <View style={styles.list}>
          <ScrollView>
            {pastRounds.map((guess, index) =>
              renderListItem(guess, pastRounds.length - index)
            )}
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center"
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: screenHeightConst > 600 ? 20 : 10,
    width: 300,
    maxWidth: "80%"
  },
  removeButton: {
    backgroundColor: "red"
  },
  box: {
    flex: 1
  },
  list: {
    borderColor: Colors.secondary,
    borderWidth: 2,
    marginTop: 20,
    width: 300,
    maxWidth: "80%",
    borderRadius: 5
  },
  listItem: {
    justifyContent: "space-around",
    flexDirection: "row"
  },
  highlight: {
    color: Colors.power,
    fontFamily: "open-sans-bold",
    fontSize: 15
  }
});

export default GameScreen;
