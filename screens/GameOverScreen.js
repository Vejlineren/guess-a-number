import React, { useState } from "react";

import {
  StyleSheet,
  View,
  Text,
  Button,
  Image,
  Dimensions
} from "react-native";

// Custom component imports
import Card from "../components/Card";
import Colors from "../constants/colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import TitleText from "../components/TitleText";
import BodyText from "../components/BodyText";
import MainButton from "../components/MainButton";

const GameOverScreen = props => {
  return (
    <View style={styles.screen}>
      <TitleText>The game is over!</TitleText>
      <View style={styles.imageContainer}>
        <Image
          source={require("../assets/images/original.png")}
          style={styles.image}
        ></Image>
      </View>
      <View style={styles.recapContainer}>
        <BodyText>
          Number of rounds was:{" "}
          <Text style={styles.highlight}>{props.roundsNumber}</Text>
        </BodyText>
        <BodyText>
          The number guessed was:{" "}
          <Text style={styles.highlight}>{props.userNumber}</Text>
        </BodyText>
      </View>
      <MainButton onPress={props.onRestart}>NEW GAME</MainButton>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  imageContainer: {
    width: Dimensions.get("window").width * 0.7,
    height: Dimensions.get("window").width * 0.7,
    borderRadius: (Dimensions.get("window").width * 0.7) / 2,
    borderWidth: 2,
    borderColor: "black",
    overflow: "hidden",
    marginVertical: 20
  },
  image: {
    height: "100%",
    width: "100%"
  },
  highlight: {
    color: Colors.power,
    fontFamily: "open-sans-bold",
    fontSize: Dimensions.get("window").height < 400 ? 12 : 15
  },
  recapContainer: {
    margin: 10,
    alignItems: "center"
  }
});

export default GameOverScreen;
