export default {
  primary: "#67597A",
  secondary: "#85BAA1",
  tertiary: "#6E8894",
  power: "#EA7317",
  positive: "#2CD34D"
};
